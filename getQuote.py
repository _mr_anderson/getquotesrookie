import sys
from googlefinance import getQuotes


def loadTickers(tickerFileName="tickerSymbols.txt"):
    """

    :param tickerFileName: The name of the file containing ticker symbols, one per line
    :return: listOfTickers, a list of ticker symbols
    """

    # create a variable, 'ticker', used as a file handle to our tickerFileName, open the file and assign it
    # to ticker
    ticker = open(tickerFileName, "r")

    # read all the lines in our file into an array called 'lines'
    lines = ticker.readlines()

    # create a new list
    listOfTickers = []

    # iterate thru the list of lines
    for line in lines:
        line = line.rstrip('\n')    # strip off the carriage return - line feed
        listOfTickers.append(line)  # append the trimmed down line to our listOfTickers list

    # always close files when you are through with them
    ticker.close()

    # return the listOfTickers we built up
    return listOfTickers


def getQuoteData(ql):
    """

    :param ql: quote list
    :return:  data, a dictionary of json formatted quote data
    """

    data = {}  # create a data dictionary to hold the results

    # iterate through the passed in list of quotes, ql
    for quote in ql:
        data[quote] = getQuotes(quote)  # call googlefinance method to get the data for the current quote
        # Note that the dictionary data is indexed by ticker symbols like data['MU'] = json data for micron quote
        # the print statement below can be uncommented for debug
        # print getQuotes(quote)

    # return the dictionary, data
    return data


def outputResultsSimple(data):
    """
    :param data: dictionary of json data
    :return: void
    """
    # pull the keys out of the dictionary, each key is a ticker symbol
    for key in sorted(data.keys()):
        print "%s \t\t %s \n" % (key, data[key][0]['LastTradePrice'])  # simply print "MU   12.97"


def outputResultsFormatted(data):
    """

    :param data: data dictionary, indexed by ticker symbol with a value of json results from google finance
    :return:
    """
    fmt = "{0:10}\t{1:20}\t{2:20}\n"    # formatted output control

    # create white space at the top of the output
    sys.stdout.write("\n\n\n")

    # print table header
    sys.stdout.write(fmt.format("Ticker", "Last Trade Price", "Trade Date/Time"))
    sys.stdout.write("-"*70)
    sys.stdout.write("\n")

    for key in sorted(data.keys()):
        sys.stdout.write(fmt.format(
            key,
            data[key][0]['LastTradePrice'],
            data[key][0]['LastTradeDateTimeLong']))

# this is where the program execution actually occurs
if __name__ == "__main__":
    quoteList = []
    quoteList = loadTickers()
    myData = {}
    myData = getQuoteData(quoteList)
    # outputResultsSimple(myData)
    outputResultsFormatted(myData)
