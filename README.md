# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
       Super simple program to pull stock quotes using the googlefinance module
       Stocks of interest should be put into a file with one line per ticker symbol, as in tickerSymbols.txt
       The user may pass in a filename of there own, if not, the default tickerSymbols is used
       
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* install the googlefinance library like this:
       python -m pip install googlefinance

* modify tickerSymbol.txt as desired


### Who do I talk to? ###

* Repo owner _mr_anderson